from locust import HttpLocust, TaskSet, task
import string
import random


class UserBehavior(TaskSet):
    @task()
    def registerUser(self):
        word = string.ascii_lowercase
        rand_bc = ''.join(random.choice(word) for i in range(3))
        rand_name = ''.join(random.choice(word) for i in range(2))
        # rand_num = ''.join(random.randint(2,9))
        register_payload = {
            "bc_user": rand_bc,
            "OrgDepartment": "org1.department1"
        }
        self.client.post("/registerUser", data=register_payload)
        print(register_payload)

        issuegarden_payload = {
            "bc_user": rand_bc,
            "garden": "1",
            "gardenName": "ลำไย",
            "owner": rand_name,
            "areas": "13ไร่ 2งาน",
            "history_use_chemical": {
                "date_final_use_chemical": "2019/7/3",
                "status": "ว่าง"
            }
        }
        self.client.post("/IssueGarden", data=issuegarden_payload)
        print(issuegarden_payload)

        issueplanyearmodel_payload = {
            "bc_user": rand_bc,
            "update_date": "2018-08-04",
            "username": "สมชาย1",
            "name": rand_name,
            "group": "ไร่",
            "garden": "1"
        }
        self.client.post("/IssuePlanYearModel", data=issueplanyearmodel_payload)
        print(issueplanyearmodel_payload)

        issueplanting_payload = {
            "bc_user": rand_bc,
            "plant_id": "2",
            "plant_name": "สวน",
            "seedtype": "qwerty",
            "garden": "1",
            "plant_date": "2018-08-04",
            "planyear_date": "2018-08-04"
        }
        self.client.post("/IssuePlanting", data=issueplanting_payload)
        print(issueplanting_payload)

        issuemanageplanting_payload = {
            "bc_user": rand_bc,
            "plant_id": "2",
            "plant_date": "2018-08-04",
            "garden": "1",
            "production_name": "รดน้ำ",
            "production_date": "2018-08-04",
            "production_factor": "ฝักบัว"
        }
        self.client.post("/IssueManagePlanting", data=issuemanageplanting_payload)
        print(issuemanageplanting_payload)

        issueharvest_payload = {
            "bc_user": rand_bc,
            "harvesting_product_date_data": "2018-09-04",
            "harvest_transform_check": "yes",
            "plant_id": "2",
            "garden": "1",
            "plant_date": "2018-08-04",
            "planyear_date": "2018-08-04",
            "quantity": [
                {
                    "quantity_grade": "5",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                },
                {
                    "quantity_grade": "4",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                },
                {
                    "quantity_grade": "3",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                },
                {
                    "quantity_grade": "2",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                },
                {
                    "quantity_grade": "1",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                },
                {
                    "quantity_grade": "weste",
                    "quantity_amount": 100,
                    "quantity_amount_sell": 50
                }
            ],
            "total": [
                {
                    "total_grade": "good",
                    "total_amount": 500
                },
                {
                    "total_grade": "bad",
                    "total_amount": 100
                }
            ]
        }
        self.client.post("/IssueHarvest", data=issueharvest_payload)
        print(issueharvest_payload)

        issueselling_payload = {
            "bc_user": rand_bc,
            "selling_grade": "a+",
            "selling_market_place": "rudsapa",
            "selling_amout": 656262,
            "garden": "1",
            "plant_date": "2018-08-04",
            "planyear_date": "2018-08-04",
            "plant_id": "2"
        }
        self.client.post("/IssueSelling", data=issueselling_payload)
        print(issueselling_payload)
    # def issuegarden(self):


class WebsiteUser(HttpLocust):
    host = "http://206.189.152.78:7777"
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 15000
